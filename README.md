# worker-assessment



## How to use

### Prerequisites:

1) MySQL

2) Python 3


### Installation:

1) Download O*NET Database from https://www.onetcenter.org/database.html#all-files and import it.

2) Install Python MySQL connector: 
    ```pip install mysql-connector-python```

3) Install Torch:

    for GPU -> ```pip install torch torchvision torchaudio --extra-index-url https://download.pytorch.org/whl/cu113 ```

    for CPU -> ```pip install torch torchvision torchaudio```

4) Install Sentence Transformers:

    ```pip install sentence-transformers```

5) Install TextBlob:
    
    ```pip install textblob```

    ```python -m textblob.download_corpora```

6) Install Fast API 

    ```pip install "fastapi[all]"```

7) Install PyPDF2

    ```pip install PyPDF2```

8) Prepare embeddings for entities in the database by running the utility in the program folder

    ```python utility.py```

9) Set the correct database name, username and password in "skills_query.py" - class QueryMysql

	```def init_db(): return mysql.connector.connect(host='host_ip', user='user', password='password', database='database name')```


### Run the program


Run ```uvicorn main:app --host 0.0.0.0 --port 80``` and in a browser open ```http://localhost/```
