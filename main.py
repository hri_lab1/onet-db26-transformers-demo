import os
from typing import Union
import uvicorn
from fastapi import FastAPI, Request, UploadFile  # , File , Form
from fastapi.responses import FileResponse
from fastapi.staticfiles import StaticFiles
from pydantic import BaseModel
from skills_query import QueryMysql
from cv_analyzer import cv_get_results, suggest
from udemy import udemy_request
from pdf2cv import text_extract


class Item(BaseModel):
    txt: str


class Suggest(BaseModel):
    resume: str
    job: str


class Evaluate(BaseModel):
    resume: str
    jobs: list


class Courses(BaseModel):
    names: list


class Missing(BaseModel):
    abilities: Union[list, None] = None
    knowledge: Union[list, None] = None
    skills: Union[list, None] = None
    tasks: Union[list, None] = None
    tech_skills: Union[list, None] = None
    tools_used: Union[list, None] = None
    work_activities: Union[list, None] = None


class Skills(BaseModel):
    abilities: Union[list, None] = None
    knowledge: Union[list, None] = None
    skills: Union[list, None] = None
    work_activities: Union[list, None] = None


q = QueryMysql()
app = FastAPI()
folder = "./static/"


@app.post("/send-cv/")
async def create_item(item: Evaluate):
    string_encode = str(item.resume).encode("ascii", "ignore")
    resume = string_encode.decode()
    resume = resume.replace("\"", "")
    jobs = item.jobs
    a = cv_get_results(resume, jobs=jobs)
    return a


@app.post("/suggest/")
async def create_item(item: Suggest):
    string_encode = str(item.resume).encode("ascii", "ignore")
    resume = string_encode.decode()
    resume = resume.replace("\"", "")
    jobs = item.job
    result = suggest(resume, jobs)
    return result


@app.post("/get-all-skills/")
async def create_item(job: Item):
    print(job.txt)
    a = q.get_all_skills(job.txt)
    return a


@app.post("/get-all-courses/")
async def create_item(missing: Missing):
    missing_elements = {
        "abilities": missing.abilities,
        "knowledge": missing.knowledge,
        "skills": missing.skills,
        "tasks": missing.tasks,
        "tech_skills": missing.tech_skills,
        "tools_used": missing.tools_used,
        "work_activities": missing.work_activities
    }

    for key in missing_elements:
        if missing_elements.get(key) is None:
            continue
        me = []
        for e in missing_elements.get(key):
            gain = e[0] - e[1]
            element = {
                "element_value": e[0],
                "element_name": e[2]
            }
            if len(e) > 3:
                element["element_description"] = e[3]
            element["additional_score"] = gain
            me.append(element)
        missing_elements[key] = me

    for key in missing_elements:
        if missing_elements.get(key) is None:
            continue
        sorted_elements = missing_elements.get(key)
        sorted_elements = sorted(sorted_elements, key=lambda x: x["additional_score"], reverse=True)
        elements = [x.get("element_name") for x in sorted_elements]

        for i, element in enumerate(elements[:5]):
            sorted_elements[i]["udemy_course"] = udemy_request(element)
        missing_elements[key] = sorted_elements

    return missing_elements


@app.post("/get-these-courses/")
async def create_item(names: Courses):
    skill_names = names.names
    courses = []
    for name in skill_names:
        course = udemy_request(name)
        courses.append(course)
    return courses


@app.post("/get-job-suggestion/")
async def create_item(skills: Skills):
    skills_elements = {
        "abilities": skills.abilities,
        "knowledge": skills.knowledge,
        "skills": skills.skills,
        "work_activities": skills.work_activities
    }

    return q.suggest(skills_elements)


@app.get("/get-jobs-categories/")
async def root():
    job_list = [x[1] for x in q.get_job_codes()]
    job_list.sort()
    return job_list


@app.get("/get-jobs-codes/")
async def root():
    return q.get_job_codes()  # job_list


@app.get("/get-course-suggestion/")
async def get_course_suggestion(query: str = ""):
    course = udemy_request(query)
    return course


@app.get("/get_skills_for_suggestion/")
async def root():
    return q.get_skills_for_suggestion()


@app.post("/uploadfile/")
async def create_upload_file(file: Union[UploadFile, None] = None):
    if not file:
        return {"message": "No upload file sent"}
    else:
        contents = await file.read()
        with open(file.filename, "wb") as binary_file:
            binary_file.write(contents)
        if file.filename.endswith(".pdf"):
            text = text_extract(file.filename)
        elif file.filename.endswith(".txt"):
            with open(file.filename, "r") as text_file:
                text = text_file.read()
        else:
            text = "File format not supported!"
        if os.path.isfile(file.filename):
            os.remove(file.filename)
        return {"filename": file.filename, "text": text}


# serve static pages
app.mount("/static", StaticFiles(directory="./static/"), name="static")


@app.get("/{catchall:path}", response_class=FileResponse)
def read_index(req: Request):
    # check first if requested file exists
    path = req.path_params["catchall"]
    if len(path) == 0:
        path = "index.html"
    file = folder + path

    print('look for: ', path, file)
    if os.path.exists(file):
        return FileResponse(file)

    # otherwise return index files
    index = 'static/index.html'
    return FileResponse(index)


proc = None


if __name__ == '__main__':
    uvicorn.run(app='main:app', host='0.0.0.0')
