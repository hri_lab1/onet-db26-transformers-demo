var skill_names;
var skill_divs = [];
var skill_keys = ["abilities", "knowledge", "skills", "work_activities"];
var levels = ["none", "little", "medium", "good", "excellent"];
var values = [0.0, 0.25, 0.5, 0.75, 1.0];
var skill_value_intervals = [6, 7, 6, 7, 5, 1, 1];
var All_Details;
var user_scores = {};
var max_skills = [0, 0, 0, 0]


// manages creation of abilities/skills section
document.addEventListener("DOMContentLoaded", function (event) {
    getJSON('/get_skills_for_suggestion/', function (err, data) {
        if (err !== null) {
            alert('Something went wrong: ' + err);
        } else {
            skill_names = data;

            display_skills()

        }
    });
});

//display input section
function display_skills() {
    skill_divs = []
    var entity_num = 0
    var skill_div = document.getElementById("skill_div")
    skill_div.style.display = "inline"
    skill_div.innerHTML = "<h3>Please select your level on the following elements:</h3>"
    for (k in skill_keys) {
        let key = skill_keys[k]
        //skill_keys.push(key);
        max_skills[entity_num] = skill_names[key].length;
        var elements = skill_names[key];
        let div = document.createElement("div");
        div.id = key;
        div.className = "skill_divs";
        skill_div.appendChild(div);

        let entity_name = key.split("_");
        for (i in entity_name) {
            entity_name[i] = entity_name[i].charAt(0).toUpperCase() + entity_name[i].slice(1);
        }
        entity_name = entity_name.join(" ");
        p = '<details>'
        p += '<summary class="details">' + entity_name + ":</summary>";
        if (skill_names[key].length == 0) {
            p += "<p>Sorry, no element found!</p>";
        }
        var names = []
        for (e in elements) {
            var name = elements[e][0]
            var description = elements[e][1];
            p += '<div class="radio-toolbar">'
            p += '<p class="element"><b>' + name + '</b>';
            if (description && description.length > 0) {
                p += ':<br>' + description + '<br>';
            } else {
                p += '<br>';
            }
            for (i in levels) {
                p += '<input type="radio" class="radio" id="' + key + "_" + name + i + '" name="' + key + "_" + name + '" value="' + values[i] + '">';
                p += '<label for="' + key + "_" + name + i + '">' + levels[i] + '</label>';
            }
            p += '</p>';
            p += '</div>';
            names.push(key + "_" + name);
        }
        p += '<p class="more" onclick="close_details()">close...</p>';
        p += '</details>';
        div.innerHTML = p;
        for (i in names) {
            document.getElementById(names[i] + 0).checked = true;
        }
        entity_num++;
    }
    details();
    document.getElementById("button4").style.display = "inline";
}

// manages the only one opened detail
function details() {
    All_Details = document.querySelectorAll('details');

    All_Details.forEach(deet => {
        deet.addEventListener('toggle', toggleOpenOneOnly)
    })

    function toggleOpenOneOnly(e) {
        if (this.open) {
            All_Details.forEach(deet => {
                if (deet != this && deet.open) {
                    deet.open = false;
                    window.scrollTo(0, 0);
                }
            });
        }
    }
}

// manages the only one opened detail
function close_details() {
    All_Details = document.querySelectorAll('details');
    All_Details.forEach(deet => {
        deet.open = false;
    })
}

// get user input from radio inputs
function get_radio_values() {
    close_details()
    //document.getElementById("skill_div").style.display = "none"
    //document.getElementById("button4").style.display = "none"

    for (k in skill_keys) {
        let key = skill_keys[k]
        var elements = skill_names[key];
        user_scores[key] = [];
        for (e in elements) {
            var name = elements[e][0]
            var description = elements[e][1];

            let radio = document.querySelector('input[name="' + key + "_" + name + '"]:checked').value;
            radio *= skill_value_intervals[k];
            user_scores[key].push(radio);
        }
    }
    suggest();
}

function suggest() {
    document.getElementById("spinner").style.display = "inline";
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/get-job-suggestion/", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(user_scores));
    xhr.onload = function () {
        var res = this.responseText;
        var jj = JSON.parse(res);

        document.getElementById("spinner").style.display = "none";
        window.scrollTo(0, 0);
        let jobs = jj.suggested_jobs;
        let div = document.getElementById("result");
        let p = "<h3>Suggested job categories</h3>";
        p += "<ol>";
        for (job of jobs) {
            p += '<div class="tooltip">'
            p += "<li>";
            p += '<p class="tooltip">' + job[1] + '</p>'
            p += '<div id="tooltipmsg" class="tooltiptext">'+ job[2] + '</div></div>'
            p += "</li>";
        }
        p += "</ol>";
        div.innerHTML = p;
    }
}

var getJSON = function (url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onload = function () {
        var status = xhr.status;
        if (status === 200) {
            callback(null, xhr.response);
        } else {
            callback(status, xhr.response);
        }
    };
    xhr.send();
};

function clear_page() {
    location.reload()
}

