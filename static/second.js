var All_Details;
var jobs;
var job_list = [];
var skills = [5, 5, 5, 5, 5, 5, 5];
var max_skills = [0, 0, 0, 0, 0, 0, 0];
var skill_keys = ["abilities", "knowledge", "skills", "work_activities", "tasks"]  //, "tools_used", "tech_skills"];
var skill_max_scores = [0, 0, 0, 0, 0, 0, 0];
var user_scores = [0, 0, 0, 0, 0, 0, 0];
var skill_course_scores = [0, 0, 0, 0, 0, 0, 0];
var skill_value_intervals = [6, 7, 6, 7, 5, 1, 1];
var skill_divs = [];
var selected_job;
var analysis_data;
var inputs;
var levels = ["none", "little", "medium", "good", "excellent"];
var values = [0.0, 0.25, 0.5, 0.75, 1.0];
var missing = {};
var user_score = 0;
var user_max_score = 0;

var job_name = document.getElementsByName("job");
job_name[0].addEventListener('input', (evt) => { update_select(document.querySelector('input').value) });


// manages job search
document.addEventListener("DOMContentLoaded", function (event) {
    getJSON('/get-jobs-categories/', function (err, data) {
        if (err !== null) {
            //alert('Something went wrong: ' + err);
        } else {
            job_list = data;
            jobs = data;

            var sel = document.getElementById('jobs')
            for (i in job_list) {
                j = job_list[i];
                var opt = document.createElement("option");
                opt.appendChild(document.createTextNode(j));
                sel.appendChild(opt)
            }
        }
    });
});


// get skill from server for the specified job
function get_skills() {
    user_scores = [0, 0, 0, 0, 0, 0, 0];
    skill_course_scores = [0, 0, 0, 0, 0, 0, 0];
    inputs = [];
    document.getElementById("spinner").style.display = "inline";
    document.getElementById("container").style.display = "none";
    document.getElementById("container2").style.display = "none";
    document.getElementById("result").innerHTML = ""
    analysis_data = {}

    let j = document.getElementById("job").value

    if (j.length == 0) {
        alert("You have to select a job...")
        document.getElementById("spinner").style.display = "none";
        return
    }
    let msg = { "txt": j }
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/get-all-skills/", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(msg));
    xhr.onload = function () {
        var res = this.responseText
        var jj = JSON.parse(res);
        for (key of skill_keys) {
            analysis_data[key] = jj[key]
        }
        document.getElementById("spinner").style.display = "none";
        compute_skills()
        window.scrollTo(0, 0);
    }
}

function compute_skills() {
    selected_job = analysis_data.job
    delete analysis_data.job
    skill_divs = []
    var entity_num = 0
    var skill_div = document.getElementById("skill_div")
    skill_div.style.display = "inline"
    skill_div.innerHTML = "<h2>Please select your level on the following elements:</h2>"
    for (k in skill_keys) {
        let key = skill_keys[k]
        //skill_keys.push(key);
        max_skills[entity_num] = analysis_data[key].length;
        var elements = analysis_data[key];
        let div = document.createElement("div");
        div.id = key;
        div.className = "skill_divs";
        skill_div.appendChild(div);

        let entity_name = key.split("_");
        for (i in entity_name) {
            entity_name[i] = entity_name[i].charAt(0).toUpperCase() + entity_name[i].slice(1);
        }
        entity_name = entity_name.join(" ");
        p = '<details>'
        p += '<summary class="details">' + entity_name + ":</summary>";
        if (analysis_data[key].length == 0) {
            p += "<p>Sorry, no element found!</p>";
        }
        var names = []
        for (e in elements) {
            var name;
            var description;
            if (key == "tech_skills" || key == "tools_used") {
                name = elements[e][1];
                description = "";

            } else if (key == "tasks") {
                name = elements[e][3];
                description = null;
            } else {
                name = elements[e][2];
                description = elements[e][3];
            }
            
            p += '<div class="radio-toolbar">'
            p += '<p class="element">' + name;
            if (description && description.length > 0) {
                p += ':<br>' + description + '<br>';
            } else {
                p += '<br>';
            }
            
            for (i in levels) {
                p += '<input type="radio" class="radio" id="' + key + "_" + name + i + '" name="' + key + "_" + name + '" value="' + values[i] + '">';
                p += '<label for="' + key + "_" + name + i + '">' + levels[i] + '</label>';
            }
            p += '</p>';
            p += '</div>';
            names.push(key + "_" + name);
        }
        p += '<p class="more" onclick="close_details()">close...</p>';
        p += '</details>';
        div.innerHTML = p;
        for (i in names) {
            document.getElementById(names[i] + 0).checked = true;
        }
        entity_num++;
    }
    details();
    document.getElementById("button4").style.display = "inline";
    document.getElementById("button5a").style.display = "none";
    document.getElementById("button5").style.display = "inline";
}

function details() {
    All_Details = document.querySelectorAll('details');

    All_Details.forEach(deet => {
        deet.addEventListener('toggle', toggleOpenOneOnly)
    })

    function toggleOpenOneOnly(e) {
        if (this.open) {
            All_Details.forEach(deet => {
                if (deet != this && deet.open) {
                    deet.open = false;
                    window.scrollTo(0, 0);
                }
            });
        }
    }
}

function close_details() {
    All_Details = document.querySelectorAll('details');
    All_Details.forEach(deet => {
        deet.open = false;
    })
}

function get_radio_values() {
    close_details()
    document.getElementById("skill_div").style.display = "none"
    document.getElementById("button4").style.display = "none"
    document.getElementById("button5a").style.display = "inline";
    document.getElementById("button5").style.display = "none";
    let score = 0;
    let total_score = 0;
    missing = {};
    for (k in skill_keys) {
        let key = skill_keys[k]
        var elements = analysis_data[key];
        var names = [];
        var scores = [];
        var user_values = [];
        var skill_missing = [];
        skill_max_scores[k] = 0;
        user_scores[k] = 0;
        for (e in elements) {
            var name;
            var elem_value;
            var description;
            if (key == "tech_skills") {
                name = elements[e][1];
                elem_value = elements[e][3] == 'Y' ? 1 : 0.75;

            } else if (key == "tools_used") {
                name = elements[e][1];
                elem_value = 1;
                description = null;

            } else if (key == "tasks") {
                name = elements[e][3];
                elem_value = elements[e][1];
                description = null;
            } else {
                elem_value = elements[e][1];
                name = elements[e][2];
                description = elements[e][3];
            }
            names.push(name);
            scores.push(elem_value);
            let radio = document.querySelector('input[name="' + key + "_" + name + '"]:checked').value;
            radio *= skill_value_intervals[k];
            score += radio > elem_value ? elem_value : radio;
            total_score += elem_value;
            skill_max_scores[k] += elem_value;
            user_scores[k] += radio > elem_value ? elem_value : radio;
            user_values.push(radio);
            if (radio < elem_value) {
                skill_missing.push([elem_value, radio, name, description]);
            }
        }
        missing[key] = skill_missing;
    }
    let p_score = score * 100 / total_score;
    user_score = score;
    user_max_score = total_score;
    display_bar(p_score, 1);
    suggest();
}

function suggest() {
    document.getElementById("spinner").style.display = "inline";
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/get-all-courses/", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(missing));
    xhr.onload = function () {
        var res = this.responseText
        var jj = JSON.parse(res);
        for (key of skill_keys) {
            missing[key] = jj[key]
        }
        document.getElementById("spinner").style.display = "none";
        window.scrollTo(0, 0);
        display_suggestions();
        display_skill_bars();
    }
}

function display_skill_bars() {
    for (k in skill_keys) {
        let key = skill_keys[k]
        let p_score = (skill_course_scores[k] + user_scores[k]) * 100 / skill_max_scores[k];
        display_bar(p_score, key);
    }
}

function display_bar(score, bar) {
    var elem;
    var container;
    var perc;
    if (bar == 1) {
        elem = document.getElementById("resultBar");
        container = document.getElementById("container")
        perc = 'perc'
    } else if (bar == 2) {
        elem = document.getElementById("resultBar2");
        container = document.getElementById("container2")
        perc = 'perc2'
    } else {
        elem = document.getElementById("resultBar_" + bar);
        container = document.getElementById("container_" + bar)
        perc = 'perc_' + bar
    }

    let eligibility;
    let color;

    let p_score = score;

    if (p_score > 100) {
        p_score = 100;
    }
    p_w = p_score

    if (p_w < 25) {
        p_w = 25
    }
    elem.style.width = p_w + "%";
    container.style.display = "inline";
    if (p_score >= 40 && p_score < 60) {
        elem.style.backgroundColor = "yellow";
        eligibility = "Almost eligible...";
        color = "black"
    } else if (p_score < 40) {
        elem.style.backgroundColor = "red"
        eligibility = "Not yet eligible...";
        color = "white"
    } else {
        elem.style.backgroundColor = "green";
        eligibility = "Eligible!";
        color = "white"
    }
    elem.innerHTML = '<p class="perc" id="' + perc + '">' + p_score.toFixed(2) + '% - ' + eligibility + '</p>';
    document.getElementById(perc).style.color = color;
}


var getJSON = function (url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onload = function () {
        var status = xhr.status;
        if (status === 200) {
            callback(null, xhr.response);
        } else {
            callback(status, xhr.response);
        }
    };
    xhr.send();
};

function update_select(job_search) {
    const { options } = document.querySelector('#jobs');
    while (options.length) {
        options[0].remove();
    }
    var sel = document.getElementById('jobs')

    for (i in job_list) {
        j = job_list[i];
        if (j.toLowerCase().includes(job_search.toLowerCase())) {
            var opt = document.createElement("option");
            opt.appendChild(document.createTextNode(j));
            sel.appendChild(opt)
        }
    }
}

function get_value() {
    var e = document.getElementById("jobs");
    var value = e.options[e.selectedIndex].value;
    var text = e.options[e.selectedIndex].text;
    document.getElementById("job").value = value;
}

function test() {
    document.getElementById("spinner").style.display = "inline";
    getJSON('skills.json', function (err, data) {
        if (err !== null) {
            alert('Something went wrong: ' + err);
        } else {
            document.getElementById("spinner").style.display = "none";
            analysis_data = data;
            //display_suggestions(data)
            compute_skills()
        }
    });
}

function check_selected_courses() {
    inputs = document.querySelectorAll("input[type=checkbox]");
    let somma = 0;
    skill_course_scores = [0, 0, 0, 0, 0, 0, 0];
    for (i in inputs) {
        if (inputs[i].checked) {
            somma += parseFloat(inputs[i].value);
            let key = inputs[i].name.split("$")[0];
            let skill = skill_keys.indexOf(key);
            skill_course_scores[skill] += parseFloat(inputs[i].value);
        }

    }
    display_bar((0.0 + user_score + somma) * 100 / user_max_score, 2);
    display_skill_bars();
}

function display_suggestions() {
    var result = document.getElementById("result");
    var con = 0;
    p = "<h2>Missing knowledge or skills by which you can raise your score</h2>";
    result.innerHTML = p
    entity_num = 0
    skill_keys = []
    for (entity in missing) {
        max_skills[entity_num] = missing[entity].length
        skill_keys.push(entity);
        let suggestions = missing[entity].slice(0, skills[entity_num]);
        let entity_name = entity.split("_");
        for (i in entity_name) {
            entity_name[i] = entity_name[i].charAt(0).toUpperCase() + entity_name[i].slice(1);
        }
        entity_name = entity_name.join(" ");
        p = '<h3 class="entity">' + entity_name + ":</h3>";
        p += '<div class="container" id="container_' + entity + '">'
        //p += '<label for="resultContainer_' + entity + '">Your results in this section: </label> <br></br>'
        p += '<div clas="resultContainer" id="resultContainer_' + entity + '">'
        p += '<div class="resultBar" id="resultBar_' + entity + '"></div></div><br></div>'
        if (missing[entity].length == 0) {
            p += "<h4>Sorry, no element found!</h4>";
        }
        result.innerHTML += p
        for (skill in suggestions) {
            p = ""
            if (suggestions[skill].element_name) {
                con++;
                p += '<details>'
                p += '<summary class="details">'
                p += '<input type="checkbox" id="check' + con + '" name="' + entity + "$" + suggestions[skill].element_name + '" value="' + suggestions[skill].additional_score + '" onchange="check_selected_courses()">'
                p += suggestions[skill].element_name + "</summary>";
                if (suggestions[skill].element_description) {
                    p += "<p>" + suggestions[skill].element_description + "</p>"
                }

                if (suggestions[skill].udemy_course && suggestions[skill].udemy_course.result == "ok") {
                    p += "<p>Udemy course recommended:<br>"
                    p += '<img src="' + suggestions[skill].udemy_course.image + '" alt="Course image">'
                    p += " " + '<a href="' + suggestions[skill].udemy_course.url + '" target="_blank">' + suggestions[skill].udemy_course.title + '</a>' + "</p>"
                } else {
                    p += "<p>Sorry, no Udemy course found for this element.</p>"
                }
                p += "</details>"
            }
            result.innerHTML += p
        }
        if (skills[entity_num] < max_skills[entity_num]) {
            result.innerHTML += '<p class="more" onclick="more(' + entity_num + ', 10)">more...</p>';
        }
        entity_num++;
    }
}

function more(ent, n) {
    let names = [];
    let entities = [];
    for (let i = 0; i < n; i++) {
        if (skills[ent] < max_skills[ent]) {
            names.push(missing[skill_keys[ent]][skills[ent]].element_name)
            entities.push(missing[skill_keys[ent]][skills[ent]])
            skills[ent]++;
        }
    }
    get_more_data(names, entities)
}

function get_more_data(names, entities) {
    document.getElementById("spinner").style.display = "inline";
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/get-these-courses/", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify({names: names}));
    xhr.onload = function () {
        var res = this.responseText
        var jj = JSON.parse(res);
        document.getElementById("spinner").style.display = "none";
        for (i in entities) {
            entities[i].udemy_course = jj[i]
        }
        display_suggestions(analysis_data);
        var new_inputs = document.querySelectorAll("input[type=checkbox]")
        for (i in inputs) {
            for (j in new_inputs) {
                if (new_inputs[j].name == inputs[i].name) {
                    new_inputs[j].checked = inputs[i].checked
                    break
                }
            }
        }
        display_skill_bars();
    }
}

function clear_page() {
    document.getElementById("job").value = ""
    location.reload()
}