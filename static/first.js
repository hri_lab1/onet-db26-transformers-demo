var jobs;
var job_list = [];
var skills = [5, 5, 5, 5, 5, 5, 5]
var max_skills = [0, 0, 0, 0, 0, 0, 0]
var skill_keys = ["abilities", "knowledge", "skills", "work_activities", "tasks", "tools_used", "tech_skills"];
skill_keys = []
var analysis_data;
var inputs;
var skill_max_scores = [0, 0, 0, 0, 0, 0, 0];
var user_scores = [0, 0, 0, 0, 0, 0, 0];
var skill_course_scores = [0, 0, 0, 0, 0, 0, 0];
var skill_complete = [0, 0, 0, 0, 0, 0, 0];

var job_name = document.getElementsByName("job")
job_name[0].addEventListener('input', (evt) => { update_select(document.querySelector('input').value) });

document.addEventListener("DOMContentLoaded", function (event) {
    getJSON('/get-jobs-categories/', function (err, data) {
        if (err !== null) {
            alert('Something went wrong: ' + err);
        } else {
            job_list = data;
            jobs = data;
            var sel = document.getElementById('jobs')
            for (i in job_list) {
                j = job_list[i];
                var opt = document.createElement("option");
                opt.appendChild(document.createTextNode(j));
                sel.appendChild(opt)
            }
        }
    });
});

function clean_text(input) {
    text_only = ''
    for (j in input) {
        // get text
        c = input.charCodeAt(j);
        if (c < 128) {
            text_only += input[j];
        }
        else {
            text_only += ' ';
        }
    }
    return text_only.replace(/  +/g, ' ')
}

function suggest() {
    document.getElementById("spinner").style.display = "inline";
    document.getElementById("container").style.display = "none";
    document.getElementById("container2").style.display = "none";
    var result = document.getElementById("result");
    result.innerHTML = "";
    let txt = document.getElementById('txt').value
    txt = clean_text(txt);
    if (txt.length == 0 || txt == ' ') {
        alert("You have to input a CV...")
        document.getElementById("spinner").style.display = "none";
        return
    }
    let j = document.getElementById("job").value

    if (j.length == 0) {
        alert("You have to select a job...")
        document.getElementById("spinner").style.display = "none";
        return
    }

    let msg = { "resume": txt, "job": j }
    document.getElementById("button").style.display = "none";
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/suggest/", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(msg));
    xhr.onload = function () {
        var res = this.responseText;
        var j = JSON.parse(res);
        analysis_data = j;
        document.getElementById("spinner").style.display = "none";
        if(j.total_max_score==0){
            display_no_data()
        } else {
            compute_matched_elements();
            display_bar(j.resume_score, 1);
            display_suggestions(j);
            display_skill_bars()
            window.scrollTo(0, 0);
        }
    }
}

function compute_matched_elements() {
    let matched = analysis_data.matched_elements;
    let maximums = analysis_data.max_scores
    let counter = 0;
    for (i in matched) {
        user_scores[counter] = 0;
        skill_max_scores[counter] = maximums[i + "_max_score"];
        for (k in matched[i]) {
            user_scores[counter] += matched[i][k][0];
        }
        counter++;
    }
}

function display_skill_bars() {

    let max_score = analysis_data.total_max_score;
    for (k in skill_keys) {
        let key = skill_keys[k]
        let factor = max_score / skill_max_scores[k]
        let p_score = (user_scores[k] / skill_max_scores[k] + 0.5 * user_scores[k] + skill_course_scores[k])
        display_bar(p_score, key);
    }
}

function display_bar(score, bar) {
    var elem;
    var container;
    var perc;
    let max_score;
    let min_score;
    let complete;
    if (bar == 1) {
        elem = document.getElementById("resultBar");
        container = document.getElementById("container")
        perc = 'perc'
        max_score = analysis_data.total_max_score * 0.5 + 1.0;
        min_score = analysis_data.minimum_score_for_eligibility;
    } else if (bar == 2) {
        let i;
        elem = document.getElementById("resultBar2");
        container = document.getElementById("container2")
        perc = 'perc2'
        max_score = analysis_data.total_max_score * 0.5 + 1.0;
        min_score = analysis_data.minimum_score_for_eligibility;
        let diff = 0;
        for (i in skill_complete) {
            diff += max_skills[i] - skill_complete[i];
        }
        complete = diff == 0;
    } else {
        elem = document.getElementById("resultBar_" + bar);
        container = document.getElementById("container_" + bar)
        perc = 'perc_' + bar
        max_score = 1.0 + analysis_data.max_scores[bar + "_max_score"] * 0.5
        min_score = analysis_data.minimum_score_for_eligibility * max_score / (analysis_data.total_max_score * 0.5 + 1);
        let i = skill_keys.indexOf(bar);
        complete = max_skills[i] - skill_complete[i] == 0
    }

    let p_score;
    let eligibility;
    let color;
    if (score <= min_score) {
        p_score = (score * 60 / min_score).toFixed(2);
    } else {
        p_score = (60 + ((score - min_score) * 40 / (max_score - min_score)));
        if (p_score <= 99.99 && !complete) {
            p_score = p_score.toFixed(2);
        } else if (complete) {
            p_score = 100.0;
        } else {
            p_score = 99.99;
        }
    }
    if (p_score > 100) {
        p_score = 100;
    }
    p_w = p_score
    if (p_score < 1) {
        p_score = 0;
    }
    if (p_w < 25) {
        p_w = 25
    }
    elem.style.width = p_w + "%";
    container.style.display = "inline";
    if (p_score >= 40 && p_score < 60) {
        elem.style.backgroundColor = "yellow";
        eligibility = "Almost eligible...";
        color = "black"
    } else if (p_score < 40) {
        elem.style.backgroundColor = "red"
        eligibility = "Not yet eligible...";
        color = "white"
    } else {
        elem.style.backgroundColor = "green";
        eligibility = "Eligible!";
        color = "white"
    }
    elem.innerHTML = '<p class="perc" id="' + perc + '">' + p_score + '% - ' + eligibility + '</p>';
    document.getElementById(perc).style.color = color;
}

var getJSON = function (url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onload = function () {
        var status = xhr.status;
        if (status === 200) {
            callback(null, xhr.response);
        } else {
            callback(status, xhr.response);
        }
    };
    xhr.send();
};

function update_select(job_search) {
    const { options } = document.querySelector('#jobs');
    while (options.length) {
        options[0].remove();
    }
    var sel = document.getElementById('jobs')

    for (i in job_list) {
        j = job_list[i];
        if (j.toLowerCase().includes(job_search.toLowerCase())) {
            var opt = document.createElement("option");
            opt.appendChild(document.createTextNode(j));
            sel.appendChild(opt)
        }
    }
}

function get_value() {
    var e = document.getElementById("jobs");
    var value = e.options[e.selectedIndex].value;
    var text = e.options[e.selectedIndex].text;
    document.getElementById("job").value = value;
}

function check_selected_courses() {
    inputs = document.querySelectorAll("input[type=checkbox]");
    let somma = 0;
    skill_course_scores = [0, 0, 0, 0, 0, 0, 0];
    skill_complete = [0, 0, 0, 0, 0, 0, 0];
    for (i in inputs) {
        if (inputs[i].checked) {
            somma += parseFloat(inputs[i].value);
            let key = inputs[i].name.split("$")[0];
            let skill = skill_keys.indexOf(key);
            skill_course_scores[skill] += parseFloat(inputs[i].value);
            skill_complete[skill] += 1;
        }

    }
    display_bar(analysis_data.resume_score + somma, 2)
    display_skill_bars();
}

function display_no_data(){
    var result = document.getElementById("result");
    var p = "<h3>I'm sorry but the data for the job category you have chosen are not yet in the database</h3>";
    result.innerHTML = p
}

function display_suggestions(data) {
    var result = document.getElementById("result");
    var con = 0;
    var p = "<h2>Missing knowledge or skills by which you can raise your score</h2>";
    result.innerHTML = p
    entity_num = 0
    skill_keys = []
    for (entity in data.missing_elements) {
        max_skills[entity_num] = data.missing_elements[entity].length
        skill_keys.push(entity);
        let suggestions = data.missing_elements[entity].slice(0, skills[entity_num]);
        let entity_name = entity.split("_");
        for (i in entity_name) {
            entity_name[i] = entity_name[i].charAt(0).toUpperCase() + entity_name[i].slice(1);
        }
        entity_name = entity_name.join(" ");
        p = '<h3 class="entity">' + entity_name + ":</h3>";
        p += '<div class="container" id="container_' + entity + '">'
        p += '<div clas="resultContainer" id="resultContainer_' + entity + '">'
        p += '<div class="resultBar" id="resultBar_' + entity + '"></div></div><br></div>'
        if (data.missing_elements[entity].length == 0) {
            p += "<h4>Sorry, no element found!</h4>";
        }
        result.innerHTML += p
        for (skill in suggestions) {
            p = ""
            if (suggestions[skill].element_name) {
                con++;
                p += '<details>'
                p += '<summary class="details">'
                p += '<input type="checkbox" id="check' + con + '" name="' + entity + "$" + suggestions[skill].element_name + '" value="' + suggestions[skill].additional_score + '" onchange="check_selected_courses()">'
                p += suggestions[skill].element_name + "</summary>";
                if (suggestions[skill].element_description) {
                    p += "<p>" + suggestions[skill].element_description + "</p>"
                }

                if (suggestions[skill].udemy_course && suggestions[skill].udemy_course.result == "ok") {
                    p += "<p>Udemy course recommended:<br>"
                    p += '<img src="' + suggestions[skill].udemy_course.image + '" alt="Course image">'
                    p += " " + '<a href="' + suggestions[skill].udemy_course.url + '" target="_blank">' + suggestions[skill].udemy_course.title + '</a>' + "</p>"
                } else {
                    p += "<p>Sorry, no Udemy course found for this element.</p>"
                }
                p += "</details>"
            }
            result.innerHTML += p
        }
        if (skills[entity_num] < max_skills[entity_num]) {
            result.innerHTML += '<p class="more" onclick="more(' + entity_num + ', 10)">more...</p>';
        }
        entity_num++;
    }
}

function more(ent, n) {
    let names = [];
    let entities = [];
    for (let i = 0; i < n; i++) {
        if (skills[ent] < max_skills[ent]) {
            names.push(analysis_data.missing_elements[skill_keys[ent]][skills[ent]].element_name)
            entities.push(analysis_data.missing_elements[skill_keys[ent]][skills[ent]])
            skills[ent]++;
        }
    }
    get_more_data(names, entities)
}

function get_more_data(names, entities) {
    document.getElementById("spinner").style.display = "inline";
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/get-these-courses/", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify({names: names}));
    xhr.onload = function () {
        var res = this.responseText
        var jj = JSON.parse(res);

        document.getElementById("spinner").style.display = "none";
        for (i in entities) {
            entities[i].udemy_course = jj[i]
        }
        display_suggestions(analysis_data);
        var new_inputs = document.querySelectorAll("input[type=checkbox]")
        for (i in inputs) {
            for (j in new_inputs) {
                if (new_inputs[j].name == inputs[i].name) {
                    new_inputs[j].checked = inputs[i].checked
                    break
                }
            }
        }
        display_skill_bars();
    }
}

async function uploadFile() {
    let formData = new FormData();
    formData.append("file", fileupload.files[0]);
    await fetch('/uploadfile/', {
        method: "POST",
        body: formData
    }).then((response) => response.json())
        .then((data) => {
            document.getElementById('txt').value = data.text
        });

}

function clear_page() {
    document.getElementById('txt').value = ""
    location.reload()
}